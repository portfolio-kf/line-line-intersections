# Line-Line Intersections Using CUDA

Small project to quickly find the points of intersection between two large collections of finite lines using parallel processing (CUDA).

## Example

The example will find the points of intersection between the finite lines *l1, l2, l3, l4* and rays *r~x~* sent out from the center of a circle *c* with a radius of 200 at an interval of 1 degree. The finite lines represent a rectangle, which is defined by the points *P1 = (-150, 150), P2 = (150, 150), P3 = (150, -150)* and *P4 = (-150, -150)*. 

Using this setup, it will be noticed that for a ray *r~i~* send out to a point *G* which lies on *c* and outside of the rectangle, a point of intersection *I* will be found; for a ray *r~j~* send out to a point *H* which lies on *c* but inside of the rectangle, no intersection will be found. In case no intersection is found, *(inf, inf)* will be returned.

Intersections are returned as a matrix. The output for this example can be found in [output.txt](output.txt). Lines *l1, l2, l3, l4* are respectively represented by the columns, the rays (send out at an interval of 1 degree) are represented by the rows.

 

![Visualization of the example](example_viz.png)

## To DO

- [x] Line line intersections using CUDA
- [ ] Error handling