#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <valarray>
#include <cuda_runtime.h>

#include "ll_intersector.h"

/**
 * Swap the values of two variables.
 *
 * @param a Variable with value a. Will have value b after calling function.
 * @param b Variable with value b. Will have value a after calling function.
 */
__device__ void dSwap(double & a, double & b)
{
    double temp = b;
    b = a;
    a = temp;
}

/** 
 * Check whether two lines overlap in 1 dimension.
 * 
 * @param a1 First point of line a.
 * @param a2 Second point of line a.
 * @param b1 First point of line b.
 * @param b2 Second point of line b.
 * @return Whether the two line do overlap.
 */
__device__ bool dOverlap(double a1, double a2, double b1, double b2)
{
    if (a1 > a2) dSwap(a1, a2);
    if (b1 > b2) dSwap(b1, b2);
    double lb = (a1 > b1) ? a1 : b1;
    double ub = (a2 < b2) ? a2 : b2;
    return lb <= ub;
}

/**
 * Get the line parameters of a certain line l.
 *
 * @param l Line to convert to LineParam.
 * @return Line l as LineParam.
 */
__device__ LineParam dToLineParam(Line l)
{
    LineParam lp;
    lp.a = l.p2.y - l.p1.y;
    lp.b = l.p1.x - l.p2.x;
    lp.c = lp.a * l.p1.x + lp.b * l.p1.y;
    return lp;
}

/**
 * Check whether a certain value is in between two values. 
 *
 * @param a Value a.
 * @param b Value b.
 * @param x Value to check whether it lies between value a and value b.
 * @return Whether value x lies between value a and value b.
 */
__device__ bool dBetween(double a, double b, double x)
{
    double EPS = 1e-3; // Used to omit rounding errors.
    if (a > b) dSwap(a, b);
    double lb = (a < b) ? a : b;
    double ub = (a > b) ? a : b;
    return (lb - EPS <= x) && (x <= ub + EPS);
}

/**
 * Find point of intersection between two sets of finite lines using CUDA parallel processing.
 *
 * @param out 2D array with size (LA, LB) to store of points points of intersection. Coordinate (i, j) corresponds to the point of interesection between a[i] and b[j].
 * @param a First set of lines with lenght LA.
 * @param b Second set of lines with lenght LB.
 */
__global__ void kLineLineIntersection(Point ** out, Line * a, Line * b)
{
    Line l1 = a[blockIdx.x];
    Line l2 = b[threadIdx.x];
    
    Point intersection;
    intersection.x = INFINITY;
    intersection.y = INFINITY;

    if (dOverlap(l1.p1.x, l1.p2.x, l2.p1.x, l2.p2.x) && dOverlap(l1.p1.y, l1.p2.y, l2.p1.y, l2.p2.y))
    {
        LineParam lp1 = dToLineParam(l1);
        LineParam lp2 = dToLineParam(l2);
        double det = lp1.a * lp2.b - lp2.a * lp1.b;
        double abs = (det < 0) ? -det : det;
        if (abs > 1e-6)
        {
            intersection.x = (lp1.c * lp2.b - lp2.c * lp1.b) / det;
            intersection.y = (lp1.a * lp2.c - lp2.a * lp1.c) / det;
        }
        if (!dBetween(l1.p1.x, l1.p2.x, intersection.x) || 
            !dBetween(l1.p1.y, l1.p2.y, intersection.y) || 
            !dBetween(l2.p1.x, l2.p2.x, intersection.x) || 
            !dBetween(l2.p1.y, l2.p2.y, intersection.y))
        {
            intersection.x = INFINITY;
            intersection.y = INFINITY;
        }
    }

    out[threadIdx.x][blockIdx.x] = intersection;    
}

/**
 * Wrapper function to find point of intersection between two sets of finite lines using CUDA parallel processing.
 * The number of blocks used is equal to the number elements in c_a, the number of threads per block is equal to the number of elements in c_b.
 * In case two lines are parallel or the point of intersection is not on the segment, (inf, inf) will be returned.
 *
 * @param c_a First set of lines with lenght n_a.
 * @param c_b Second set of lines with lenght n_B.
 * @param n_a Number of elements in set c_a.
 * @param n_b Number of elements in set c_b.
 * @param intersections 2D array with size (n_a, n_b) to store of points points of intersection. Coordinate (i, j) corresponds to the point of interesection between c_a[i] and c_b[j].
 */
__host__ void LineLineIntersections(Line* c_a, Line* c_b, int n_a, int n_b, Point** intersections)
{
    Line * dev_a, * dev_b;
    Point ** dev_intersections, ** host_dev_intersections = new Point*[n_b];
    
    cudaMalloc(&dev_a, n_a * sizeof(Line));
    cudaMalloc(&dev_b, n_b * sizeof(Line));
    cudaMalloc(&dev_intersections, n_b * sizeof(Point *));
    for (int i = 0; i < n_b; i++) 
        cudaMalloc(&host_dev_intersections[i], n_a * sizeof(Point));


    cudaMemcpy(dev_a, c_a, n_a * sizeof(Line), cudaMemcpyHostToDevice);    
    cudaMemcpy(dev_b, c_b, n_b * sizeof(Line), cudaMemcpyHostToDevice);    
    cudaMemcpy(dev_intersections, host_dev_intersections, n_b * sizeof(Point *), cudaMemcpyHostToDevice);

    dim3 num_blocks(n_a);
    dim3 threads_per_block(n_b);
    kLineLineIntersection<<<num_blocks, threads_per_block>>>(dev_intersections, dev_a, dev_b);

    for (int i = 0; i < n_b; i++) 
        cudaMemcpy(intersections[i], host_dev_intersections[i], n_a * sizeof(Point), cudaMemcpyDeviceToHost);


    cudaFree(&dev_a);
    cudaFree(&dev_b);
    for (int i = 0; i < n_b; i++) 
        cudaFree(&host_dev_intersections[i]);
    cudaFree(&dev_intersections);

    delete host_dev_intersections;
}


int main()
{
    // Define corner points of rectangle.
    Point p1, p2, p3, p4;
    p1.x = -150; p1.y =  150;  
    p2.x =  150; p2.y =  150;
    p3.x =  150; p3.y = -150;
    p4.x = -150; p4.y = -150;
    
    // Define lines of rectangle.
    Line l1, l2, l3, l4;
    l1.p1 = p1;  l1.p2 = p2;
    l2.p1 = p2;  l2.p2 = p3;
    l3.p1 = p3;  l3.p2 = p4;
    l4.p1 = p4;  l4.p2 = p1;

    // Define rays sent from center of the circle to the circle.
    Point center; center.x = 0; center.y = 0;
    double radius = 200;
    double rad[360];
    for (int i = 0; i < 360; i++) rad[i] = i * (M_PI / 180);
    std::valarray<double> rad_arr (rad, 360);
    std::valarray<double> y_val = sin(rad_arr) * radius;
    std::valarray<double> x_val = cos(rad_arr) * radius;

    // Define collections
    Line c_a[] = { l1, l2, l3, l4 };
    Line c_b[360];
    for (int i = 0; i < 360; i++)
    {
        Line l;
        l.p1 =  center;
        l.p2.x = x_val[i]; l.p2.y = y_val[i];
        c_b[i] = l;
    }

    // Get size of collections
    int n_a = sizeof(c_a)/sizeof(Line);
    int n_b = sizeof(c_b)/sizeof(Line);
    
    // Reserve memory to store return values.
    Point** c_return = new Point*[n_b];
    for (int i = 0; i < n_b; i++) c_return[i] = new Point[n_a];

    // Find intersections
    LineLineIntersections(&c_a[0], &c_b[0], n_a, n_b, c_return);
    
    // Print results
    for (int y = 0; y < n_b; y++) 
    {
        std::cout << y << ":\t";
        for (int x = 0; x < n_a; x++) 
        {
            std::cout << "(" << c_return[y][x].x << ", " << c_return[y][x].y << ") ";
        }
        std::cout << std::endl;
    }

    // Clean up memory
    for (int i = 0; i < n_b; i++) delete c_return[i];
    delete c_return;

    return 0;
}