#ifndef UTIL
#define UTIL

struct Point { double x, y; };
struct Line { Point p1, p2; };
struct LineParam { double a, b, c; };


#endif